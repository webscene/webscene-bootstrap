@file:Suppress("unused")

const val rootProjectName = "webscene-bootstrap"
val localRepoDir by lazy { "${System.getProperty("user.home")}/.m2/repository" }

object Version {
    const val kotlin = "1.2.60"
    const val dokka = "0.9.14"
    const val websceneCore = "0.1"
}

object Dependency {
    const val dokkaPlugin = "org.jetbrains.dokka:dokka-gradle-plugin:${Version.dokka}"
    const val websceneCoreCommon = "org.webscene:webscene-core-common:${Version.websceneCore}"
    const val websceneCoreJvm = "org.webscene:webscene-core-jvm:${Version.websceneCore}"
    const val websceneCoreJs = "org.webscene:webscene-core-js:${Version.websceneCore}"
}

object PluginId {
    const val dokka = "org.jetbrains.dokka"
    const val kotlinPlatformCommon = "kotlin-platform-common"
    const val kotlinPlatformJvm = "kotlin-platform-jvm"
    const val kotlinPlatformJs = "kotlin-platform-js"
    const val kotlin2Js = "kotlin2js"
}
