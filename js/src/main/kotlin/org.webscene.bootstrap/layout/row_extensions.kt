@file:Suppress("PackageName", "PackageDirectoryMismatch")

package org.webscene.bootstrap.layout.row_ext

import org.w3c.dom.Element
import org.webscene.bootstrap.layout.Row
import org.webscene.core.html.element.toDomElement

fun Row.toDomElement(): Element {
    updateAttributes()
    return toDomElement()
}
