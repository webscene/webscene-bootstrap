@file:Suppress("unused")

package org.webscene.bootstrap.layout

import org.webscene.bootstrap.layout.column_ext.toDomElement
import org.webscene.bootstrap.layout.container_ext.toDomElement
import org.webscene.bootstrap.layout.row_ext.toDomElement
import org.webscene.core.html.element.childTagHandlers

fun initBootstrapLayout() {
    childTagHandlers += { parent, childTag ->
        when (childTag) {
            is Container -> parent.appendChild(childTag.toDomElement())
            is Row -> parent.appendChild(childTag.toDomElement())
            is Column -> parent.appendChild(childTag.toDomElement())
        }
    }
}
