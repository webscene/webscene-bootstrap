@file:Suppress("PackageName", "PackageDirectoryMismatch")

package org.webscene.bootstrap.layout.column_ext

import org.w3c.dom.Element
import org.webscene.bootstrap.layout.Column
import org.webscene.core.html.element.toDomElement

fun Column.toDomElement(): Element {
    updateAttributes()
    return toDomElement()
}
