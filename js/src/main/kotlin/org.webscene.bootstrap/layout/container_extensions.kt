@file:Suppress("PackageName", "PackageDirectoryMismatch")

package org.webscene.bootstrap.layout.container_ext

import org.w3c.dom.Element
import org.webscene.bootstrap.layout.Container
import org.webscene.core.html.element.toDomElement

fun Container.toDomElement(): Element {
    updateAttributes()
    return toDomElement()
}
