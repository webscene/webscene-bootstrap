// TODO: Uncomment the line below.
//import org.jetbrains.dokka.gradle.DokkaTask

val moduleName = "$rootProjectName-${project.name}"

plugins {
    `maven-publish`
}

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri(localRepoDir) }
}

// TODO: Remove the block below.
//publishing {
//    publications {
//        create("docs", MavenPublication::class.java) {
//            from(components["java"])
//            artifact(createDokkaJar)
//        }
//        create("sources", MavenPublication::class.java) {
//            from(components["java"])
//            artifact(createSourceJar)
//        }
//    }
//
//    repositories {
//        maven { url = uri("$buildDir/repository") }
//    }
//}

buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(kotlin(module = "gradle-plugin", version = Version.kotlin))
        classpath(Dependency.dokkaPlugin)
    }
}

apply {
    plugin(PluginId.dokka)
    plugin(PluginId.kotlinPlatformCommon)
    from("${rootProject.rootDir.absolutePath}/publishing.gradle")
}

dependencies {
    "compile"(kotlin(module = "stdlib-common", version = Version.kotlin))
    "compile"(Dependency.websceneCoreCommon)
}

// TODO: Uncomment the block below.
/*
val dokka by tasks.getting(DokkaTask::class) {
    this.moduleName = moduleName
    outputDirectory = "$buildDir/javadoc"
    sourceDirs = files("src/main/kotlin")
    doFirst { File("${projectDir.absolutePath}/build/javadoc").deleteRecursively() }
}
val createDokkaJar by tasks.creating(Jar::class) {
    dependsOn(dokka)
    classifier = "javadoc"
    from(dokka.outputDirectory)
}
*/
val createSourceJar by tasks.creating(Jar::class) {
    baseName = moduleName
    dependsOn("classes")
    classifier = "sources"
    from("src/main/kotlin")
}
val jar by tasks.getting(Jar::class) {
    baseName = moduleName
}

task("createAllJarFiles") {
    // TODO: Uncomment the line below.
//    dependsOn("jar", createSourceJar, createDokkaJar)
    // TODO: Remove the line below.
    dependsOn("jar", createSourceJar)
    println("Creating $moduleName JAR files (library, sources and documentation)...")
    doLast { println("Finished creating JAR files.") }
}
