package org.webscene.bootstrap.style

/**
 * Contains all available BootstrapLayout Button styles.
 * @property txt Text representation of the button style.
 */
enum class ButtonStyle {
    /** Standard button. **/
    DEFAULT,
    /** Identifies a primary action. **/
    PRIMARY,
    SUCCESS,
    INFO,
    WARNING,
    DANGER,
    /** Turns the button into a link. **/
    LINK
}

val ButtonStyle.txt
    get() = "btn-${name.toLowerCase()}"
