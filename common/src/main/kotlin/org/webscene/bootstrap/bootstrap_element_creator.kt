package org.webscene.bootstrap

import org.webscene.bootstrap.layout.Column
import org.webscene.bootstrap.layout.ColumnSize
import org.webscene.bootstrap.layout.Container
import org.webscene.bootstrap.layout.Row

// Creates BootstrapLayout elements.

internal fun createBootstrapContainer(block: Container.() -> Unit): Container {
    val containerElement = Container()
    containerElement.block()
    return containerElement
}

internal fun createBootstrapRow(block: Row.() -> Unit): Row {
    val rowElement = Row()
    rowElement.block()
    return rowElement
}

internal fun createBootstrapColumn(colSizes: Array<Pair<ColumnSize, Int>>, block: Column.() -> Unit): Column {
    val colElement = Column()
    colElement.block()
    colSizes.forEach { (colSizeType, amount) -> colElement.colSizes[colSizeType] = amount }
    return colElement
}
