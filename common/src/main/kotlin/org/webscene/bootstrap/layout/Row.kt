package org.webscene.bootstrap.layout

import org.webscene.core.html.element.ParentHtmlElement

/**
 * BootstrapLayout Row element.
 * @see org.webscene.core.html.element.ParentHtmlElement
 */
class Row : ParentHtmlElement() {
    @Suppress("RedundantSetter")
    override var tagName
        get() = "div"
        set(_) {}

    /**
     * Creates a new [column][Column] in [Row] that can contain HTML elements.
     * @param colSizes One or more column sizes to use for sizing the column.
     * @param block Initialisation block for setting up the column.
     * @return A new [Column].
     */
    @Suppress("unused")
    fun column(vararg colSizes: Pair<ColumnSize, Int>, block: Column.() -> Unit): Column {
        val colElement = Column()
        children += colElement
        colElement.block()
        colSizes.forEach { (sizeType, amount) -> colElement.colSizes[sizeType] = amount }
        return colElement
    }

    override fun updateAttributes() {
        super.updateAttributes()
        classes += "row"
    }

    override fun createText(indent: Int): String {
        updateAttributes()
        return super.createText(indent)
    }
}
