package org.webscene.bootstrap.layout

/**
 * Contains all available BootstrapLayout Button sizes.
 * @property txt Text representation of the button size.
 */
enum class ButtonSize(val txt: String) {
    LARGE("btn-lg"),
    SMALL("btn-sm"),
    EXTRA_SMALL("btn-xs"),
    /** Make the button use the full width of the parent. **/
    FULL_WIDTH("btn-block")
}
