package org.webscene.bootstrap.layout

/**
 * Contains all available BootstrapLayout column sizes.
 * @property txt Text representation of the column size.
 */
enum class ColumnSize(val txt: String) {
    /** Smartphone size. **/
    EXTRA_SMALL("col-xs"),
    /** Tablet size. **/
    SMALL("col-sm"),
    /** Desktop size. **/
    MEDIUM("col-md"),
    /** Desktop size. **/
    LARGE("col-lg")
}
