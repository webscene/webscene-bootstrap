package org.webscene.bootstrap.layout

import org.webscene.core.html.element.ParentHtmlElement

/**
 * BootstrapLayout Column element.
 * @see org.webscene.core.html.element.ParentHtmlElement
 */
class Column : ParentHtmlElement() {
    /** Contains column sizes. Each entry is [column size type][ColumnSize] (key), span size (value). */
    val colSizes = mutableMapOf<ColumnSize, Int>()
    @Suppress("RedundantSetter")
    override var tagName
        get() = "div"
        set(_) {}

    override fun updateAttributes() {
        super.updateAttributes()
        colSizes.keys.forEach { classes += "${it.txt}-${colSizes[it]}" }
    }

    override fun createText(indent: Int): String {
        updateAttributes()
        return super.createText(indent)
    }
}
