package org.webscene.bootstrap.layout

import org.webscene.core.html.element.ParentHtmlElement

/**
 * BootstrapLayout Container element.
 * @see org.webscene.core.html.element.ParentHtmlElement
 */
class Container : ParentHtmlElement() {
    @Suppress("RedundantSetter")
    override var tagName
        get() = "div"
        set(_) {}
    /** [Container] uses the full width of the screen if set to true. **/
    var fullWidth = true

    /**
     * Creates a new [row][Row] in [Container] that holds one or more columns.
     * @param block Initialisation block for setting up the row.
     * @return A new [Row].
     */
    @Suppress("unused")
    fun row(block: Row.() -> Unit): Row {
        val rowElement = Row()
        children += rowElement
        rowElement.block()
        return rowElement
    }

    override fun createText(indent: Int): String {
        updateAttributes()
        return super.createText(indent)
    }

    override fun updateAttributes() {
        super.updateAttributes()
        classes += if (fullWidth) "container-fluid" else "container"
    }
}
